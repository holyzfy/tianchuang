# 商户引流

## 目录结构 

* `www` 项目根目录
* `tools` AMD构建脚本

## tomcat配置
0. 请把项目放到webapps目录下
0. 端口：8080
0. [禁用缓存](https://gist.github.com/holyzfy/24bb3418f4cf832b329c)，或者在浏览器调试工具的网络面板里禁用缓存

## 预览
[http://localhost:8080/tianchuang/www/](http://localhost:8080/tianchuang/www/)

## 发布
请使用[febu](https://github.com/holyzfy/febu)

## 单元测试
[http://localhost:8080/tianchuang/www/test/unit.html](http://localhost:8080/tianchuang/www/test/unit.html)

## 功能测试
0. 安装firefox插件[Selenium IDE](http://docs.seleniumhq.org/projects/ide/)
0. 打开Selenium IDE，文件 &gt; open 选择`selenium/test_suite.html`，点击按钮**Play entire test suite**
