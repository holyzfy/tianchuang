define(function(require) {
	var $ = require('jquery');
	var template = require('template');
	var pagination = require('jquery.pagination');

	var urlMap = function() {
		if(typeof app !== 'object') return;

		app.urlMap = app.urlMap || {};

		// 查看运行环境
		var getEnv = function() {
			var devList = [
				'127.0.0.1',
				'localhost'
			];
			var isLocal = $.inArray(location.hostname, devList) > -1;
			if(app.env === 'debug') {
				return 'debug';
			} else if(isLocal) {
				return 'localhost';
			} else {
				return 'production';
			}
		};
		
		var thisEnv = getEnv();
		if(thisEnv === 'localhost' || thisEnv === 'debug') {
			// debug环境下的mock路径相对于www/test/unit.html来计算的
			var mockBaseUrl = thisEnv === 'localhost' ? '' : '../';
			app.urlMap = {
				'userInfo': mockBaseUrl + 'mock/user_info.json',
				'product': mockBaseUrl + 'mock/product.json',
				'buy_action': mockBaseUrl + '',
				'captcha': mockBaseUrl + '',
				'help_menu': mockBaseUrl + 'mock/help_menu.json',
				'help_content': mockBaseUrl + 'mock/help_content.json',
				'loan_action': mockBaseUrl + '',
				'my_benefit': mockBaseUrl + 'mock/mybenefit.json',
				'productlist': mockBaseUrl + 'mock/productlist.json',
				'benefit': mockBaseUrl + 'mock/productlist.json',
				'redirectUrl': mockBaseUrl + '_todo_url'
			};
		}
	}

	var ajax = function(options, callback) {
		var params = $.extend({
			dataType: "json"
		}, options);

		var promise = $.ajax(params);

		var fail = function(err) {
			$(window).trigger('fail', [params]);
		};

		promise.done(function(data) {
			if(data.data && data.data.redirectUrl) {
				location.href = data.data.redirectUrl;
			} else {
				callback(data);
			}
		});
		promise.fail(fail);

		return promise;
	};

	var apiGet = function(url, data, callback) {
		var callback = arguments[arguments.length - 1];
		return ajax({
			url: url,
			data: data,
		}, callback);
	};

	var apiPost = function(url, data, callback) {
		var callback = arguments[arguments.length - 1];
		return ajax({
			url: url,
			data: data,
			type: 'post', // jQuery 1.9.0 之前的版本，你需要使用type选项
			method: 'post'
		}, callback);
	};

	var handleFail = function() {
		$(window).on('fail', function(e, err) {
			if(typeof console === 'object' && typeof console.error === 'function') {
				console.error('系统异常', err);
			}
		});
	};

	var pagination = function(elem, data, callback) {
		$(elem).pagination(data.count, {
            current_page: data.current,
            items_per_page: data.limit,
            num_edge_entries: 1,
            num_display_entries: 7,
            prev_text: '&lt;',
            next_text: '&gt;',
            load_first_page: false,
            // prev_show_always: false,
            // next_show_always: false,
            callback: callback
        });
	};

	var templateHelper = function() {
		if(typeof template != 'function') return;
		
		template.helper('parseInt', function(value) {
			return parseInt(value);
		});

		template.helper('getPeriodUnit', function(value) {
			return (value.match(/[^\d]+/) || [])[0];
		});
		
		/** 
		 * 对日期进行格式化， 
		 * @param date 要格式化的日期 
		 * @param format 进行格式化的模式字符串
		 *     支持的模式字母有： 
		 *     y:年, 
		 *     M:年中的月份(1-12), 
		 *     d:月份中的天(1-31), 
		 *     h:小时(0-23), 
		 *     m:分(0-59), 
		 *     s:秒(0-59), 
		 *     S:毫秒(0-999),
		 *     q:季度(1-4)
		 * @return String
		 * @author yanis.wang
		 * @see	http://yaniswang.com/frontend/2013/02/16/dateformat-performance/
		 */
		template.helper('dateFormat', function (date, format) {

		    date = new Date(parseInt(date));
		    var map = {
		        "M": date.getMonth() + 1, //月份 
		        "d": date.getDate(), //日 
		        "h": date.getHours(), //小时 
		        "m": date.getMinutes(), //分 
		        "s": date.getSeconds(), //秒 
		        "q": Math.floor((date.getMonth() + 3) / 3), //季度 
		        "S": date.getMilliseconds() //毫秒 
		    };
		    format = format.replace(/([yMdhmsqS])+/g, function(all, t){
		        var v = map[t];
		        if(v !== undefined){
		            if(all.length > 1){
		                v = '0' + v;
		                v = v.substr(v.length-2);
		            }
		            return v;
		        }
		        else if(t === 'y'){
		            return (date.getFullYear() + '').substr(4 - all.length);
		        }
		        return all;
		    });
		    return format;
		});
	};

	// 在页眉右侧显示用户信息
	var profile = function() {
		var exist = app.urlMap && app.urlMap.userInfo;
		exist && apiGet(app.urlMap.userInfo, function(data){
			if(!data.data) return;
			var username = data.data.fullname;
			$('#header_user em').text(username);
		});
	};

	// 导航的选中状态
	var nav = function() {
		$('#nav').children('[data-role=' + app.page + ']').addClass('selected');
	};

	var init = function() {
		urlMap();
		handleFail();
		templateHelper();
	};

	var start = function() {
		nav();
		profile();
	};

	return {
		apiGet: apiGet,
		apiPost: apiPost,
		pagination: pagination,
		init: init,
		start: start
	};

});