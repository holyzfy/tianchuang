define(function(require) {
	var $ = require('jquery');
	var template = require('template');
	var common = require('common');
	var benefitTmpl = require('text!view/benefit.html');
	var recommendTmpl = require('text!view/recommend.html');
	
	// 福利专区
	var benefit = function() {
		common.apiGet(app.urlMap.benefit, function(data) {
			var list = data.data.list[0];
			$.extend(list, {
				_ctxpath: app.ctxpath
			});
			var html = template.compile(benefitTmpl)(list);
			$('#benefit').html(html);
		});
	};

	// 精选推荐
	var list = function() {
		common.apiGet(app.urlMap.productlist, function(data) {
			$.extend(data.data, {
				_ctxpath: app.ctxpath
			});
			var list = template.compile(recommendTmpl)(data.data);
			$('#recommend_bd').html(list);
		});
	};

	var start = function() {
		benefit();
		list();
	};

	return {
		start: start
	};

});