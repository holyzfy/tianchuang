define(function(require) {
	var $ = require('jquery');
	var template = require('template');
	var common = require('common');
	var tmpl = require('text!view/my_benefit.html');

	var render = function(callback) {
		common.apiGet(app.urlMap['my_benefit'], function(data) {
			var html = template.compile(tmpl)(data.data);
			$('#content').html(html);
			callback && callback();
		});
	};

	var start = function(){
		render(function(){
			$('#my_list div.item_toggle').click(function(e){
				e.preventDefault();
				$(this).closest('li').toggleClass('selected');
			});
		});
	};

	return {
		start: start
	};

});