define(function(require){
	var $ = require('jquery');
	var Raphael = require('raphael');
	var highcharts = require('highcharts_more');
	var template = require('template');
	var common = require('common');
	var recommendTmpl = require('text!view/recommend.html');

	var getData = function() {
		common.apiGet(app.urlMap.userInfo, function(data){
			if(!data.data) return;
			
			pointer(data.data);
			polar(data.data);
		});
	};

	var getLevel = function(score) {
		if(score >= 800) return '优秀';
		if(score >= 600) return '良好';
		if(score >= 200) return '一般';
		return '较差';
	};

	// 信用分
	var pointer = function(data) {
		var $container = $('#credit_pointer');
		var containerWidth = $container.width();
		var containerHeight = $container.height();
		var score = data.score;

		var src = $('#credit_pointer_img').attr('src');

		if(isNaN(score)) {
			return;
		}

		$('#credit_score_bd').text(score);

		var levelTmpl = '你的信用{{level}}，评估时间：{{date | dateFormat:"yyyy/MM/dd"}}';
		var levelData = {
			level: getLevel(score),
			date: data.createDate
		};
		var levelHtml = template.compile(levelTmpl)(levelData);
		$('#credit_score_ft').text(levelHtml);
		
		// 180deg : 1000分
		// 用183deg是为了能在1000分时，指针恰好偏满
		var ratio = 183 / 1000;
		var deg = score * ratio;

		var pointerWidth = 128;
		var pointerHeight = 30;
		var centerX = containerWidth / 2 - pointerWidth;
		var centerY = containerHeight - pointerHeight;
		var paper = Raphael("credit_pointer", containerWidth, containerHeight);
		var pointer = paper.image(src, centerX, centerY, pointerWidth, pointerHeight);
		var originX = containerWidth / 2;
		var originY = containerHeight - pointerHeight / 2;
		var animation = Raphael.animation({transform: ['R', deg, originX, originY]}, 1000, centerY);
		pointer.animate(animation);
	};

	// 雷达图
	var polar = function(data) {
		var thisCategories = [];
		var thisData = [];
		var getScoreName = function(key) {
			var map = {
				'LIABILITY': '资产负债',
				'OPERATION_CAPABILITY': '运营能力',
				'INCREASE_FUTURE': '未来增长',
				'COMPANY_HONOUR': '企业名誉',
				'RELATE_HONOUR': '相关人信誉'
			};
			return map[key];
		};
		$.each(data.scoreList, function(i, item){
			thisCategories.push(getScoreName(this.scoreName));
			thisData.push(this.score);
		});

		$('#credit_radar').highcharts({
			credits: {
				enabled: false
			},
			title: {
			    text: ''
			},
	        chart: {
	            polar: true,
	            type: 'line',
	        },
	        title: {
	            text: null
	        },
	        xAxis: {
	            categories: thisCategories,
	            tickmarkPlacement: 'on',
	            lineWidth: 0
	        },
	        yAxis: {
	            gridLineInterpolation: 'polygon',
	            lineWidth: 0,
	            min: 0
	        },
	        tooltip: {
	        	formatter: function() {
	        		return '<span>' + this.x + ':' + this.point.y + '</span>';
	        	}
	        },
	        series: [{
	        	showInLegend: false,
	            data: thisData,
	            pointPlacement: 'on',
	            color: '#0db1a0',
	            lineWidth: 1
	        }]

	    });
	};

	// 近七日年化收益率
	var chart = function() {
		if(typeof chartData === 'undefined') return;

		$('#bao_chart_bd').highcharts({
			credits: {
				enabled: false
			},
			title: {
			    text: ''
			},
			xAxis: {
	            type: 'datetime',
	            dateTimeLabelFormats: {
	                day: '%m.%d'
	            },
	            title: {
	                text: ''
	            },
	            tickInterval: 24 * 3600 * 1000
	        },
			yAxis: {
	            title: {
	                text: ''
	            },
	            labels: {
	                formatter: function() {
	                    return this.value + '%';
	                }
	            }
	        },
	        tooltip: {
	        	crosshairs: [true],
	        	formatter: function() {
	        		var d = new Date(this.x);
	        		var month = d.getMonth() + 1;
	        		var date = d.getDate();

	        		return month + '月' + date + '日: <strong>' + this.y + '%</strong>';
	        	}
	        },
	        series: [{
	        	showInLegend: false,
	        	pointStart: chartData.pointStart,
	        	pointInterval: 24 * 3600 * 1000,
	            data: chartData.data,
	            color: '#0db1a0'
	        }]
	    });
	};

	// 精选理财推荐
	var product = function() {
		common.apiGet(app.urlMap.productlist, function(data) {
			$.extend(data.data, {
				_ctxpath: app.ctxpath
			});
			var list = template.compile(recommendTmpl)(data.data);
			$('#recommend_bd').html(list);
		});
	};

	var start = function(){
		chart();
		product();
		getData();
	};

	var debug = {
		getLevel: getLevel
	};

	return {
		start: start,
		_debug: debug
	};

});