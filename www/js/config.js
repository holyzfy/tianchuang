require.config({
	waitSeconds: 0,
	baseUrl: 'js',
	paths: {
		'jquery': 'lib/jquery',
		'raphael': 'lib/raphael',
		'highcharts': 'lib/highcharts',
		'highcharts_more': 'lib/highcharts_more',
		'template': 'lib/template',
		'text': 'lib/text',
		'view': '../templates',
		'url': 'lib/url',
		'jquery.pagination': 'lib/jquery.pagination',
		'jquery.event.drag': 'lib/jquery.event.drag',
		'jquery.validate': 'lib/jquery.validate'
	},
	shim: {
		'highcharts': ['jquery'],
		'highcharts_more': ['highcharts'],
		'url': {
			exports: 'url'
		},
		'jquery.pagination': ['jquery'],
		'jquery.event.drag': ['jquery'],
		'jquery.validate': ['jquery'],
		'jquery.validate_common': ['jquery.validate']
	}
});