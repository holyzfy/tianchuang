define(function(require) {
	var $ = require('jquery');
	require('jquery.validate_common');
	var common= require('common');
	var url = require('url');

	var form = function() {
		var busy = false;

		var $form = $('#buy_form');
		var validator = $form.validate();

		var captcha = function() {
			var $field = $('#field_captcha');
			var $btn = $('#buy_checkcode_btn');
			var timer;
			var wait = false;
			var _count = 90; // 90秒后重试
			var count = _count;
			var btnText = $btn.text();

			var fail = function(msg) {
				msg = $.type(msg) === 'string' ? msg : '获取验证码失败，请稍后重试';
				alert(msg);
			};

			$btn.click(function() {
				if(busy || wait) return;

				var mobileIsValid = validator.element('#field_mobile');

				if(!mobileIsValid) return;

				var promise = common.apiGet(app.urlMap.captcha, {
					mobile: $.trim($('#field_mobile').val())
				}, function(data) {
					if(data.status === 0) {
						wait = true;
						alert('短信已发送，请查收');
						$btn.addClass('disabled').text('请等待' + (count--) + '秒');
						timer = setInterval(function() {
							if(count >= 0) {
								$btn.text('请等待' + count + '秒');
								count--;
							} else {
								clearInterval(timer);
								$btn.removeClass('disabled').text(btnText);
								count = _count;
								wait = false;
							}
						}, 1000);
					} else {
						fail(data.message);
					}
				});
				promise.fail(fail);
			});
		};

		captcha();

		var projectId = url('?projectId');
		var source = url('?source');
		$form.find('input[name=projectId]').val(projectId);
		$form.find('input[name=source]').val(source);

		$form.submit(function(e) {
			e.preventDefault();

			if(busy || !$form.valid()) return;

			var params = $form.serializeArray();
			common.apiPost(app.urlMap['buy_action'], params, function(data) {
				if(data.status === 0) {
					alert('提交成功');
				} else {
					var hasErr = data.data && $.isPlainObject(data.data.errors);
					if(hasErr) {
						validator.showErrors(data.data.errors);
					} else {
						alert(data.message || '系统异常');
					}
				}
			});
		});
	};

	var start = function() {
		form();
	}

	return {
		start: start
	};
	
});