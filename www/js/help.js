define(function(require) {
	var $ = require('jquery');
	var common = require('common');
	var template = require('template');
	var url = require('url');
	var menuTmpl = require('text!view/help_menu.html');
	var contentTmpl = require('text!view/help_main.html');

	var menu = function() {
		// 菜单
		common.apiGet(app.urlMap['help_menu'], function(data) {
			if(data.status !== 0) return;

			var html = template.compile(menuTmpl)(data.data);
			$('#menu_bd').html(html);
		});

		// 展开/收起
		$('#menu_bd').on('click', 'dt', function(e) {
			$('#menu_bd dl.selected').removeClass('selected');
			$(this).closest('dl').addClass('selected');
		});

		// 正文
		var showContent = function(id) {
			var params = {
				Id: id
			};
			common.apiGet(app.urlMap['help_content'], params, function(data) {
				if(data.status !== 0) return;

				var html = template.compile(contentTmpl)(data.data.helpTopic);
				$('#main').html(html);
			});
		};

		$('#menu_bd').on('click', 'dd a', function(e) {
			e.preventDefault();
			
			$('#menu_bd dd.selected').removeClass('selected');
			$(this).closest('dd').addClass('selected');
			var id = $(this).data('id');
			showContent(id);
		});

		showContent(url('?Id'));
	};

	var start = function() {
		menu();
	};

	return {
		start: start
	};
	
});