define(function(require) {
	var $ = require('jquery');
	require('jquery.event.drag');
	require('jquery.validate_common');
	var common= require('common');

	var slider = function() {
		$('#loan div.loan_item').each(function() {
			var $this = $(this);
			var $field = $this.find('input');
			var $progress = $this.find('div.loan_progress');
			var $handle = $this.find('div.loan_handle');
			var handleWidth = $handle.outerWidth();
			var fieldData = {
				min: parseInt($field.attr('min')),
				step: parseInt($field.attr('step'))
			};
			var progressData = {
				min: $progress.data('min'),
				max: $progress.data('max'),
				step: $progress.data('step') // 每次步进的像素
			};
			var timer;

			var updateProgress = function(value) {
				if(value < progressData.min || value > progressData.max) return;

				$handle.css({
					left: value
				});
				$progress.width(value);
			};

			$field.on({
				focus: function() {
					timer = setInterval(function(){
						var value = $.trim($field.val());
						value = parseInt(value);
						if(isNaN(value)) return;

						updateProgress(value / fieldData.step * progressData.step);
					}, 300);
				},
				blur: function() {
					clearInterval(timer);
				}
			});

			$handle.drag(function(ev, dd) {
				var offsetX = Math.round( dd.offsetX / progressData.step ) * progressData.step;
				var left = Math.min(Math.max(progressData.min, offsetX), progressData.max);
				var ratio = Math.floor(left / progressData.step);
				var value = fieldData.step * ratio;

				updateProgress(left);
				$field.val(value);
			}, { relative: true });
		});
	};

	var form = function() {
		var busy = false;

		var $form = $('#loan_form');
		var validator = $form.validate({
			messages: {
				useType: '请选择',
				mortgage: '请选择'
			}
		});

		$form.submit(function(e) {
			e.preventDefault();

			if(busy || !$form.valid()) return;

			var params = $form.serializeArray();
			common.apiPost(app.urlMap['loan_action'], params, function(data) {
				if(data.status === 0) {
					alert('提交成功');
				} else {
					var hasErr = data.data && $.isPlainObject(data.data.errors);
					if(hasErr) {
						validator.showErrors(data.data.errors);
					} else {
						alert(data.message || '系统异常');
					}
				}
			});
		});
	};

	var start = function() {
		slider();
		form();
	};

	return {
		start: start
	};
	
});