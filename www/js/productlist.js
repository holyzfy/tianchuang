define(function(require) {
	var $ = require('jquery');
	var template = require('template');
	var common = require('common');
	var listTmpl = require('text!view/productlist.html');
	var url = require('url');

	var getParams = function() {
		var map = {
			source: url('?source'),
			interestFloor: url('?interestFloor'),
			interestCelling: url('?interestCelling'),
			periodDaysFloor: url('?periodDaysFloor'),
			periodDaysCelling: url('?periodDaysCelling'),
			progressFloor: url('?progressFloor'),
			sort: url('?sort'),
			orderType: url('?orderType'),
			page: url('?page')
		};
		$.each(map, function(key, value){
			map[key] = value || '';
		});
		return map;
	};

	var start = function() {
		var params = getParams();

		var goPage = function(args) {
			location.href = '?' + $.param(args);
		};

		common.apiGet(app.urlMap['productlist'], params, function(data) {
			var thisData = $.extend({}, data.data, params, {
				_ctxpath: app.ctxpath
			});
			var html = template.compile(listTmpl)(thisData);
			$('#content').html(html);

			common.pagination('#pagination', data.data, function(index, $page) {
				// index是下标，从0开始，所以要加1
				params.page = index + 1;
				goPage(params);
			});
		});

		// 条件筛选
		$('#content').on('click', 'li a', function(e) {
			e.preventDefault();

			var $this = $(this);
			var name = $this.closest('li').attr('data-name');
			var value = $this.attr('data-value') || '';
			var thisParam = {};
			if(name === 'interest') {
				var pair = value.split('-');
				thisParam.interestFloor = pair[0] || '';
				thisParam.interestCelling = pair[1] || '';
			} else if(name === 'periodDays') {
				var pair = value.split('-');
				thisParam.periodDaysFloor = pair[0] || '';
				thisParam.periodDaysCelling = pair[1] || '';
			} else {
				thisParam[name] = value;
			}
			var thisData = $.extend({}, params, thisParam);
			goPage(thisData);
		});

		// 排序
		$('#content').on('click', 'a.item_order', function(e) {
			e.preventDefault();

			var $this = $(this);
			var name = $this.attr('data-name');
			var value = $this.attr('data-value');

			var thisParam = {};
			thisParam[name] = value;
			thisParam.orderType = $this.hasClass('item_asc') ? 'desc' : 'asc';

			var thisData = $.extend({}, params, thisParam);
			goPage(thisData);
		});

		// 列表整行可点击
		$('#content').on('click', 'tbody tr', function(e) {
			if($(e.target).is('a')) {
				return;
			} else {
				var url = $(this).find('a.item_project').attr('href');
				location.href = url;
			}

		});
	};

	return {
		start: start,
		_debug: {
			getParams: getParams
		}
	};

});