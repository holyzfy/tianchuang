define(function(require){
	var $ = require('jquery');

	$(function() {
		var thisYear = (new Date).getFullYear();
		$('#this_year').text(thisYear);
	});
});