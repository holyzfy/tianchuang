(function(win) {
	var this$;

	var loadJs = function (src, callback) {
	    var head = document.getElementsByTagName("head")[0] || document.documentElement;
		var node = document.createElement("script");
		node.charset = "utf-8";
		node.src = src;
		var done = false;
		node.onload = node.onreadystatechange = function() {
		    if ( !done && (!this.readyState ||
		            this.readyState === "loaded" || this.readyState === "complete") ) {
		        done = true;
		        callback && callback();

		        node.onload = node.onreadystatechange = null;
		        head && node.parentNode && head.removeChild( node );
		    }
		};
		head.insertBefore( node, head.firstChild );
	};

	var loadCss = function (src) {
		var node = document.createElement("link");
        node.href = src;
        node.type = "text/css";
        node.rel = "stylesheet";
        var head = document.head || document.getElementsByTagName("head")[0];
        head.appendChild(node);
	};

	var init = function() {
		var guideSrc = '//tctj.yeepay.com/finance/f2e/tianchuang/style/guide-2577f8d941.css';
		loadCss(guideSrc);

		var scripts = document.getElementsByTagName("script");
		var thisScript = scripts[scripts.length - 1];
		var src = thisScript.getAttribute("src") || '';
		var id = (src.match(/[\?&]?id=([^&]*)&?/) || [])[1] || '';
		var userName;
		try {
			userName = $.trim($.trim($('#header div.topc p').text()).match(/([^\[]+)\[/)[1]);
		} catch(e) {
			win.console && console.log('get userName error');
		}
		var params = {
			sysSign: '001',
			sysUserId: id,
			userName: userName
		};

		if(typeof win.jQuery === 'function' && jQuery.fn.jquery >= "1.7.0") {
			this$ = jQuery;
			start(params);
		} else {
			var jquerySrc = '//www.yeepay.com/public/javascripts/common/jquery-1.7.1.min.js';
			var hasjQuery = !!win.jQuery;
			loadJs(jquerySrc, function() {
				$.noConflict();
				this$ = jQuery;
				if(hasjQuery) {
					jQuery = $;
				} else {
					delete win.jQuery;
				}
				start(params);
			});
		}
	};

	var validate = function(params) {
		var $ = this$;

		var url = '//tctj.yeepay.com/finance/auth/validate?callback=?';
		// var url = 'mock/validate.json';

		$.extend(params, {
			domainName: location.hostname
		});

		return $.getJSON(url, params);
	};

	var getAction = function(params) {
		var $ = this$;

		var url = '//tctj.yeepay.com/finance/toShowFloat?callback=?';
		// var url = 'mock/toShowFloat.json';

		return $.getJSON(url, params);
	};

	var start = function(params) {
		var $ = this$;

		$.when(validate(params), getAction(params)).done(function(valid, action) {
			if(valid[0].status == 0 && action[0].status == 0) {
				var type = action[0].data.type // 1是理财 2是贷款
				if(type == 1) {
					finance({
						href: '//tctj.yeepay.com/finance/wealthProduct/toProductRecommend'
					});
				} else if(type == 2) {
					loan({
						href: '//tctj.yeepay.com/finance/loanApply/toLoanApply'
					});
				}
			}
		});
	};

	/**
	 * @param {Object.<href>}
	 */
	var finance = function(data) {
		var tmpl = '<div class="yp__guidebox yp__guidehide" id="yp__guideMbox"> <div class="yp__guidecont"> <div class="yp__info yp__smaf">企业经营借款（5年保证，还款可靠）<strong>12%</strong>年化利率<a id="yp__close" href="javascript:;"></a></div> <div class="yp__topbg"></div> <div class="yp__conbg yp__prop"><dl>轻松理财<br />“升升”不息</dl><a id="yp__go" href="{{href}}" target="_blank">我有闲钱，帮我理财</a></div> <div class="yp__bombg"></div> <div class="yp__info yp__smaf">个人借款（双重保障，还款可靠）<strong>8%</strong>年化利率</div> </div> <a class="yp__guidebrie" id="yp__guideBriefly" href="javascript:;"> <em class="yp__prop"></em> <span>低风险理财</span> </a> </div>';
		var html = tmpl.replace(/\{\{href\}\}/g, data.href);
		$(html).appendTo('body');
		
		$('#yp__close').click(function(e) {
			e.preventDefault();
			$('#yp__guideMbox').addClass('yp__guidehide');
		});

		$('#yp__guideBriefly').click(function(e) {
			e.preventDefault();
			$('#yp__guideMbox').removeClass('yp__guidehide');
		});
	};

	/**
	 * @param {Object.<href>}
	 */
	var loan = function(data) {
		var tmpl = '<div class="yp__guidebox yp__guidehide" id="yp__guideMbox"> <div class="yp__guidecont"> <div class="yp__info">资金紧张不用怕,有信用就能贷款<a id="yp__close" href="javascript:;"></a></div> <div class="yp__topbg"></div> <div class="yp__conbg yp__loan">您已获得<span>&nbsp;</span>特权<a href="{{href}}" target="_blank">我想申请贷款</a></div> </div> <a class="yp__guidebrie" id="yp__guideBriefly" href="javascript:;"> <em class="yp__loan"></em> <span>无抵押贷款</span> </a> </div>'; 
		var html = tmpl.replace(/\{\{href\}\}/g, data.href);
		$(html).appendTo('body');
		
		$('#yp__close').click(function(e) {
			e.preventDefault();
			$('#yp__guideMbox').addClass('yp__guidehide');
		});

		$('#yp__guideBriefly').click(function(e) {
			e.preventDefault();
			$('#yp__guideMbox').removeClass('yp__guidehide');
		});
	};

	init();

})(this);