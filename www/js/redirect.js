define(function(require) {
	var common = require('common');
	var $ = require('jquery');
	var url = require('url');

	$(function() {
		var forwardUrl = url('?forwardUrl');
		forwardUrl && setTimeout(function() {
			location.href = forwardUrl;
		}, 10000);
	});
});