define(function(require) {
	var $ = require('jquery');
	var template = require('template');
	var common = require('common');
	var url = require('url');
	var tmpl = require('text!view/product.html');

	var render = function(callback) {
		var params = {
			Id: url('?Id')
		};
		common.apiGet(app.urlMap['product'], params, function(data) {
			var html = template.compile(tmpl)(data.data);
			$('#content').html(html);
			callback && callback();
		});
	};

	var tab = function() {
		var $tabItems = $('#tab_bd').children('.tab_item');

		$('#tab_hd a').each(function(i) {
			$(this).click(function(e) {
				e.preventDefault();
				
				$('#tab_hd a').add($tabItems).removeClass('selected');
				$(this).add($tabItems.eq(i)).addClass('selected');
			});
		});
	};

	var start = function() {
		render(tab);
	};

	return {
		start: start
	};

});