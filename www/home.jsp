<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>易宝天创金融服务中心</title>
<link rel="stylesheet" href="style/common.css" _group="all">
<link rel="stylesheet" href="style/index.css" _group="all">
<%@include file="inc/head_static.jsp" %>
</head>

<body>
<script>
app.page = 'index';
</script>

<%@include file="inc/header.jsp" %>

<div class="wrapper section credit">
	<h2 class="section_hd"><span>天星商户分</span></h2>
	<div class="section_bd credit_bd clearfix">
		<div class="credit_score">
			<div class="credit_score_hd">
				<img src="images/pointer.png" alt="" id="credit_pointer_img" style="display: none;">
				<div id="credit_pointer" class="credit_pointer"></div>
			</div>
			<div id="credit_score_bd" class="credit_score_bd"></div>
			<div id="credit_score_ft" class="credit_score_ft"></div>
		</div>

		<div id="credit_radar" class="credit_radar"></div>
	</div>
</div>

<!--  
<div class="wrapper section bao">
	<h2 class="section_hd"><span>生财宝</span></h2>
	<div class="section_bd bao_bd clearfix">
		<div class="bao_about">
			<div class="bao_about_hd clearfix">
				<img src="images/huaxia.png" alt="">
				<strong>+ 懒猫金服</strong>
			</div>
			<ul class="bao_summary">
				<li><label>七日年化收益率 :</label><strong class="item_week">5.72%</strong></li>
				<li><label>万份日收益 :</label><strong class="item_average">0.93元</strong></li>
			</ul>
		</div>
		
		<div class="bao_chart">
			<h3>近七日年化收益率</h3>
			<div id="bao_chart_bd" class="bao_chart_bd"></div>
			<script>
			var chartData = {
				// 起始日期（UNIX时间戳）
				pointStart: 1433347200000,

				// 近七日年化收益率
	            data: [3.3, 4.5, 3.8, 4.0, 1.9, 3.5, 5.4 ]
			};
			</script>
		</div>

		<div class="bao_price">
			<div class="item item_first bao_yesterday">
				<h3>昨日收益</h3>
				<strong class="item_none">暂无收益</strong>
			</div>
			<div class="item bao_total">
				<h3>总金额（元）</h3>
				<strong>￥904.50</strong>
			</div>
		</div>
	</div>
</div>
-->

<div class="wrapper section recommend">
	<h2 class="section_hd"><span>精选理财推荐</span></h2>
	<div class="section_bd">
		<ul class="clearfix" id="recommend_bd"></ul>
	</div>
</div>

<%@include file="inc/footer.jsp" %>
<script>
app.urlMap['productlist'] =  '${ctxpath}/wealthProduct/findList?projectStatus=IPB';

require(['common'], function() {
	require(['index'], function(index) {
		index.start();
	});
});
</script>
</body>
</html>