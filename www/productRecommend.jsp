<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>易宝天创金融服务中心</title>
<link rel="stylesheet" href="style/common.css" _group="all">
<link rel="stylesheet" href="style/productlist.css" _group="all">
<%@ include file="inc/head_static.jsp" %>
</head>

<body>
<script>
app.page = 'wealth';
</script>

<%@ include file="inc/header.jsp" %>

<div class="wrapper">
	<ul class="tab clearfix">
		<li class="selected"><a href="">精选推荐</a></li>
		<li><a href="${ctxpath}/wealthProduct/toProductList">理财产品</a></li>
		<li><a href="${ctxpath}/myBenefit/toMyBenefit">我的收益</a></li>
	</ul>

	<div id="benefit" class="benefit"></div>

	<div class="recommend">
		<h2 class="recommend_hd"><span>精选推荐</span></h2>

		<ul id="recommend_bd" class="recommend_bd clearfix"></ul>
	</div>

	<%@ include file="inc/feature.jsp" %>

</div>

<%@ include file="inc/footer.jsp" %>
<script>
// 福利专区
app.urlMap['benefit'] = '${ctxpath}/wealthProduct/findList?projectStatus=IPB';

// 精选推荐
app.urlMap['productlist'] = '${ctxpath}/wealthProduct/findList?projectStatus=IPB';


require(['common'], function() {
	require(['productlist_recommend'], function(recommend) {
		recommend.start();
	});
});
</script>
</body>
</html>