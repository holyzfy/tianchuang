define(function(require){
    var Qunit = require('qunit');
    var productList = require('productlist');
    var getParams = productList._debug.getParams;
    
    Qunit.test('productlist', function(assert) {
        var params = getParams();
        assert.notOk(params.source);
    });
});