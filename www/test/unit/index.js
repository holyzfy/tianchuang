define(function(require){
    var Qunit = require('qunit');
    var index = require('index');
    var getLevel = index._debug.getLevel;
    
    Qunit.test('index', function(assert) {
        assert.equal(getLevel(900), '优秀');
        assert.equal(getLevel(800), '优秀');
        assert.equal(getLevel(700), '良好');
        assert.equal(getLevel(300), '一般');
        assert.equal(getLevel(50), '较差');
    });
});