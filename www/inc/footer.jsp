<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="footer">
	<div class="footer_nav">
		<a href="http://www.yeepay.com/" target="_blank">易宝支付官网</a> |
		<a href="http://www.ypcredit.com/" target="_blank">易宝天创官网</a> |
		<a href="http://www.ypcredit.com/#page5" target="_blank">关于天创</a> |
		<a href="http://www.yeepay.com/category/map" target="_blank">网站地图</a> | 
		<a href="http://www.yeepay.com/category/aboutYeepay/contactus/联系我们" target="_blank">联系我们</a>
	</div>
	<div class="footer_copyright">2014-<span id="this_year"></span> 天创信用服务有限公司版权所有</div>
	<script>
	app.urlMap = app.urlMap || {};
	app.urlMap['userInfo'] = '${ctxpath}/userInfo/findOwn';
	
	require(['common'], function(common) {
		common.init();
		common.start();
		require(['footer']);
	});
	</script>
</div>