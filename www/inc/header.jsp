<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="header">
	<div class="wrapper clearfix">
		<a href="${ctxpath}/home" class="logo">易宝天创金融服务中心</a>
		
		<ul id="nav" class="nav">
			<li data-role="index"><a href="${ctxpath}/home">首页</a></li>
			<li data-role="wealth"><a href="${ctxpath}/wealthProduct/toProductRecommend">我要理财</a></li>
			<li data-role="loan"><a href="${ctxpath}/loanApply/toLoanApply">我要贷款</a></li>
			<!-- <li data-role="bao"><a href="">生财宝</a></li> -->
		</ul>

		<div class="header_account">
			<div id="header_user" class="header_user">
				<img src="images/_temp/profile.png" alt="">
				<em></em>
			</div>
			<div class="header_menu">
				<div class="header_menu_bd">
					<a href="${ctxpath}/helpCenter/toHelp?Id=1" class="header_help" target="_blank">帮助中心</a>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
app.ctxpath = '${ctxpath}';

if(app.ctxpath.slice(-1) === '/') {
	app.ctxpath = app.ctxpath.slice(0, -1);
}
</script>