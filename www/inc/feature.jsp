<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="feature">
	<h2 class="feature_hd">【易宝天创】为您精选推荐高收益高安全性的理财产品，助力您的财富增长</h2>
	<ul class="feature_bd clearfix">
		<li class="item_high">
			<strong>高收益</strong>
			<p>预期年化收益<br>高达8%-13%</p>
		</li>
		<li class="item_safe">
			<strong>强安全</strong>
			<p>保障投资者<br>资金安全</p>
		</li>
		<li class="item_free">
			<strong>零费用</strong>
			<p>不收取任何<br>手续费、管理费</p>
		</li>
		<li class="item_simple">
			<strong>低门槛</strong>
			<p>百元起投<br>投资理财好选择</p>
		</li>
	</ul>
</div>