<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>易宝天创金融服务中心</title>
<link rel="stylesheet" href="style/common.css" _group="all">
<link rel="stylesheet" href="style/productlist.css" _group="all">
<%@ include file="inc/head_static.jsp" %>
</head>

<body>
<script>
app.page = 'wealth';
</script>

<%@ include file="inc/header.jsp" %>

<div class="wrapper">
	<ul class="tab clearfix">
		<li><a href="${ctxpath}/wealthProduct/toProductRecommend">精选推荐</a></li>
		<li class="selected"><a href="">理财产品</a></li>
		<li><a href="${ctxpath}/myBenefit/toMyBenefit">我的收益</a></li>
	</ul>

	<div id="content"></div>

	<div id="pagination"></div>

	<%@ include file="inc/feature.jsp" %>

</div>

<%@ include file="inc/footer.jsp" %>
<script>
app.urlMap['productlist'] = '${ctxpath}/wealthProduct/findList?projectStatus=IPB';

require(['common'], function() {
	require(['productlist'], function(productlist) {
        productlist.start();
    });
});
</script>
</body>
</html>