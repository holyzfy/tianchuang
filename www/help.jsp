<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>易宝天创金融服务中心</title>
<link rel="stylesheet" href="style/common.css" _group="all">
<link rel="stylesheet" href="style/help.css" _group="all">
<%@ include file="inc/head_static.jsp" %>
</head>

<body>
<%@ include file="inc/header.jsp" %>

<div class="wrapper">
	<div class="help">
		<div class="menu">
			<div class="help_logo"><span>帮助中心</span></div>
			<div id="menu_bd"></div>
		</div>

		<div id="main" class="main"></div>
	</div>
</div>

<%@ include file="inc/footer.jsp" %>
<script>
app.urlMap['help_menu'] = '${ctxpath}/helpCenter/findAll';
app.urlMap['help_content'] = '${ctxpath}/helpCenter/find';

require(['common'], function() {
	require(['help'], function(help) {
        help.start();
    });
});
</script>
</body>
</html>