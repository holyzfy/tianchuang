<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>易宝天创金融服务中心</title>
<link rel="stylesheet" href="style/common.css" _group="all">
<%@ include file="inc/head_static.jsp" %>
</head>

<body>
<script>
app.page = 'wealth';
</script>

<%@ include file="inc/header.jsp" %>

<div class="redirect">
	<h1><span>理财产品用户信息确认</span></h1>
	<div class="redirect_bd">
		<p>正在为您跳转芝麻金融投资理财平台，请稍后...</p>
		<div class="item_ft">您的手机号将作为该平台的登录名，初始密码为zmjr+身份证号后6位，请您登录后尽快修改密码<br></div>
	</div>
</div>
<script>
require(['common'], function() {
	require(['redirect']);
})
</script>
</body>
</html>