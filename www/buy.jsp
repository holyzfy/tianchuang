<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>易宝天创金融服务中心</title>
<link rel="stylesheet" href="style/common.css" _group="all">
<link rel="stylesheet" href="style/buy.css" _group="all">
<%@ include file="inc/head_static.jsp" %>
</head>

<body>
<script>
app.page = 'wealth';
</script>

<%@ include file="inc/header.jsp" %>

<div class="wrapper">
	<h1 class="buy_title"><span>理财产品用户信息确认</span></h1>

	<form id="buy_form" action="" method="post">
		<ul class="buy">
			<li>
				<label class="buy_label">购买人姓名：</label>
				<div class="field_wrap">
					<input name="fullname" type="text" class="field" required max-length="20">
				</div>
			</li>
			<li>
				<label class="buy_label">身份证号码：</label>
				<div class="field_wrap">
					<input name="IdNo" type="text" class="field" required data-rule-idno="true" maxlength="18">
				</div>
			</li>
			<li>
				<label class="buy_label">手机号码：</label>
				<div class="field_wrap">
					<input id="field_mobile" name="mobile" type="text" class="field" required digits maxlength="11" data-rule-mobile="true">
					<span class="form_tip">（此号码将绑定交易通知，请您填写常用手机号）</span>
				</div>
			</li>
			<li class="buy_checkcode clearfix">
				<label class="buy_label">验证码：</label>
				<div class="buy_checkcode_field">
					<div class="field_wrap">
						<input id="field_captcha" name="captcha" type="text" class="field" maxlength="20" required>
					</div>
				</div>
				<button id="buy_checkcode_btn" class="buy_checkcode_btn" type="button">获取短信验证码</button>
			</li>
			<li class="item_last">
				<button class="form_submit" type="submit">确认购买</button>
			</li>
		</ul>
		<input type="hidden" name="projectId">
		<input type="hidden" name="source">
	</form>

</div>

<%@ include file="inc/footer.jsp"%>
<script>
app.urlMap['buy_action'] = '${ctxpath}/investment/investment';
app.urlMap['captcha'] = '${ctxpath}/smsVerification/send';

require(['common'], function() {
	require(['buy'], function(buy) {
		buy.start();
	});
});
</script>
</body>
</html>