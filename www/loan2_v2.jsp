<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>易宝天创金融服务中心</title>
<link rel="stylesheet" href="style/common.css" _group="all">
<link rel="stylesheet" href="style/loan2.css" _group="all">
<%@ include file="inc/head_static.jsp" %>
</head>

<body>
<script>
app.page = 'loan';
</script>

<%@ include file="inc/header.jsp" %>

<div class="wrapper">
	<h1 class="loan_title">【易宝天创】为您提供可靠高效的贷款渠道，解决您的燃眉之急</h1>
	
	<div class="loan2_logo">
		<span>阳光保险集团财产保险</span>
	</div>

	<div class="loan2_apply clearfix">
		<img src="images/sinosig_finance.png" alt="" class="item_aside">
		
		<div class="item_main">
			<h2>阳光时贷保</h2>

			<ul>
				<li>
					<label>产品介绍：</label>
					<p>“阳光时贷保”是阳光集团推出的协助广大工薪阶层和私营业主进行无抵押贷款的信用保证保险产品，<br>只要投保成功，投保人即可申请由合作金融机构发放的小额贷款。<br>无需要抵押， 无需要投保，投保便捷，手续简单期限灵活，审批迅速。</p>
				</li>
				<li>
					<label>贷款对象：</label>
					<p>自雇人士</p>
				</li>
				<li>
					<label>贷款条件：</label>
					<p>年龄在21-55周岁，<br>在申请时单位经营满一年 即可轻松申请！</p>
				</li>
			</ul>

			<div class="item_form">
				<form action="http://microfinance.sinosig.com/loan/loanApply.jsp" target="_blank" method="post">
					<input type="hidden" name="productType" value="privateEn">
					<button id="loan2_btn_detail" type="button">查看详情</button>
					<button class="item_submit" type="submit">立即申请</button>
				</form>
			</div>
		</div>
	</div>

	<div id="loan2_content" class="loan2_content">
		<div class="loan2_section loan2_feature">
			<h2 class="loan2_section_hd">产品特点</h2>
			<ul>
				<li><em>用途多</em>生意周转、业务拓展、购物、旅行、结婚等均可申请</li>
				<li><em>快放贷</em>资料齐全、最快一天放款</li>
				<li><em>易申请</em>只要您年龄在21-55周岁，在申请时单位经营满一年，即可申请</li>
				<li><em>省时省力</em>专业人员全程服务，帮助您轻松获取无贷款抵押</li>
			</ul>
		</div>

		<div class="loan2_section loan2_material">
			<h2 class="loan2_section_hd">申请材料</h2>
			<div class="loan2_material_bd">
				<ul>
					<li><div class="item_bd"><span>1</span>保险单</div></li>
					<li><div class="item_bd"><span>2</span>身份证明</div></li>
					<li><div class="item_bd"><span>3</span>工作证明</div></li>
					<li><div class="item_bd"><span>4</span>其他相关证明</div></li>
				</ul>
			</div>
		</div>
	</div>

</div>

<%@ include file="inc/footer.jsp" %>

<script>
require(['common'], function() {
	var $ = require('jquery');

	$('#loan2_btn_detail').click(function() {
		$('#loan2_content').slideToggle();
	});
});
</script>
</body>
</html>