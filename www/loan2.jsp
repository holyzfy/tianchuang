<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>易宝天创金融服务中心</title>
<link rel="stylesheet" href="style/common.css" _group="all">
<link rel="stylesheet" href="style/loan.css" _group="all">
<%@ include file="inc/head_static.jsp" %>
</head>

<body>
<script>
app.page = 'loan';
</script>

<%@ include file="inc/header.jsp" %>

<div class="wrapper">
	<h1 class="loan_title">【易宝天创】为您提供可靠高效的贷款渠道，解决您的燃眉之急</h1>
	
	<form id="loan_form" action="" method="post">
		<div id="loan" class="section loan">
			<h2 class="section_hd">申请贷款</h2>
			
			<div class="section_bd loan_bd">
				<div class="loan_item loan_count">
					<div class="loan_field">
						<div class="field_wrap">贷款金额 <input name="loanTotal" type="number" min="0" max="500000" step="10000" required> 元</div>
					</div>
					<div class="loan_slider">
						<div class="loan_progress" data-min="0" data-max="800" data-step="16">
							<div class="loan_handle"></div>
						</div>
						<div class="loan_scale">
							<div class="loan_scale_bd">
								<span class="loan_count_item_1">1万元</span>
								<span>10万元</span>
								<span>20万元</span>
								<span>30万元</span>
								<span>40万元</span>
								<span>50万元（以上）</span>
							</div>
						</div>
					</div>
				</div>

				<div class="loan_item loan_period">
					<div class="loan_field">
						<div class="field_wrap">贷款期限 <input name="duration" type="number" min="0" max="24" step="1" required> 月</div>
					</div>
					<div class="loan_slider">
						<div class="loan_progress" data-min="0" data-max="820" data-step="34">
							<div class="loan_handle"></div>
						</div>
						<div class="loan_scale">
							<div class="loan_scale_bd">
								<span class="loan_period_item_1">1个月</span>
								<span class="loan_period_item_3">3个月</span>
								<span>6个月</span>
								<span>12个月</span>
								<span>18个月</span>
								<span>24个月</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="section contact">
			<h2 class="section_hd">联系方式</h2>

			<div class="section_bd contact_bd">
				<ul>
					<li class="clearfix">
						<label class="form_txt">联系人：</label>
						<div class="item_wrap">
							<div class="field_wrap">
								<input name="linkman" type="text" class="field" required max-length="20">
							</div>
						</div>
						<div class="contact_chk">
							<span class="field_wrap">
								<label class="contact_tip">贷款用途：</label>
								<label class="contact_label"><input type="radio" name="useType" value="1" required> 企业经营</label>
								<label class="contact_label"><input type="radio" name="useType" value="2" required> 个人用户</label>
								<label class="contact_label"><input type="radio" name="useType" value="3" required> 其他</label>
							</span>
						</div>
					</li>
					<li class="clearfix">
						<label class="form_txt">联系电话：</label>
						<div class="item_wrap">
							<div class="field_wrap">
								<input name="tel" type="text" class="field" required maxlength="20">
							</div>
						</div>
						<div class="contact_chk">
							<span class="field_wrap">
								<label class="contact_tip">能否提供抵押物：</label>
								<label class="contact_label"><input type="radio" name="mortgage" value="1" required> 能提供</label>
								<label class="contact_label"><input type="radio" name="mortgage" value="0" required> 不能提供</label>
							</span>
						</div>
					</li>
				</ul>
				<div class="contact_submit_wrap">
					<button class="form_submit" type="submit">立即申请</button>
				</div>
			</div>
		</div>
	</form>

</div>

<%@ include file="inc/footer.jsp" %>

<script>
// 表单post的地址
app.urlMap['loan_action'] = '';

require(['common'], function() {
	require(['loan'], function(loan) {
		loan.start();
	});
});
</script>
</body>
</html>