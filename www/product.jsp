<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>易宝天创金融服务中心</title>
<link rel="stylesheet" href="style/common.css" _group="all">
<link rel="stylesheet" href="style/product.css" _group="all">
<%@ include file="inc/head_static.jsp" %>
</head>

<body>
<script>
app.page = 'wealth';
</script>

<%@ include file="inc/header.jsp" %>

<div id="content" class="wrapper"></div>

<%@ include file="inc/footer.jsp" %>
<script>
app.urlMap['product'] = '${ctxpath}/wealthProduct/find';

require(['common'], function() {
	require(['product'], function(product) {
        product.start();
    });
});
</script>
</body>
</html>