{
	appDir: '../www',
	mainConfigFile: '../www/js/config.js',
	dir: '../build',
	inlineText: true,
	optimize: 'none',
	modules: [
		{
			name: 'common',
			include: [
				'jquery',
				'raphael',
				'highcharts',
				'highcharts_more',
				'template',
				'text',
				'url',
				'jquery.pagination',
				'jquery.event.drag',
				'common',
				'footer',
				'jquery.validate',
				'jquery.validate_common'
			]
		},
		{
			name: 'buy',
			exclude: ['common']
		},
		{
			name: 'index',
			exclude: ['common']
		},
		{
			name: 'loan',
			exclude: ['common']
		},
		{
			name: 'my_benefit',
			exclude: ['common']
		},
		{
			name: 'product',
			exclude: ['common']
		},
		{
			name: 'productlist',
			exclude: ['common']
		},
		{
			name: 'productlist_recommend',
			exclude: ['common']
		},
		{
			name: 'help',
			exclude: ['common']
		}
	]
}